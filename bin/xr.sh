#!/bin/sh

#remove broken part monitor on the right side
xrandr --output eDP-1 --mode 1920x1080 --panning 1920x1080 && sleep 0.2
xrandr --output eDP-1 --mode 1920x1080 --panning 1920x900 --transform 1,0,0,0,1,0,0,0,-180

#--left-of, --right-of, --above, --below, --same-as another-output
#--off
#xrandr --output eDP --auto --rotate normal --pos 0x0 --output VGA --auto --rotate left --right-of LVDS
