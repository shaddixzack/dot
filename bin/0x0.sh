#!/bin/sh

dir="$(date +%H%M%S)" && mkdir /tmp/"$dir" && cp -r "${@}" /tmp/"$dir"
7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on -p"yrpswd" /tmp/"$dir".7z /tmp/"$dir" >/dev/null
curl -F "file=@/tmp/$dir.7z" 0x0.st | tee /dev/tty | qrencode -t UTF8i

