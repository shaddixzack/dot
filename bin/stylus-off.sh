#!/bin/sh
#temp workaround for svd13236pgb stylus problem making the cursor jump to corner

id="$(xinput | grep -i "stylus pen" | cut -f2 | cut -d\= -f2)" && xinput --disable "$id" && printf "id=%s DISABLED!\n" "$id"
