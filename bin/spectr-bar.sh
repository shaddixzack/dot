#!/bin/sh

##netsp() { i=wlp2s0; c=1; R1=$(cat /sys/class/net/"$i"/statistics/rx_bytes); T1=$(cat /sys/class/net/"$i"/statistics/tx_bytes); sleep 1; R2=$(cat /sys/class/net/"$i"/statistics/rx_bytes); T2=$(cat /sys/class/net/"$i"/statistics/tx_bytes); TBPS=$(expr $T2 - $T1); RBPS=$(expr $R2 - $R1); TKBPS=$(expr $TBPS / 1024); RKBPS=$(expr $RBPS / 1024); echo "$1 u:$TKBPS kB/s d:$RKBPS kB/s" ;}
#netsp() { I=wlp2s0; SLP=1; R1="$(cat /sys/class/net/"$I"/statistics/rx_bytes)"; T1="$(cat /sys/class/net/"$I"/statistics/tx_bytes)"; sleep $SLP ; R2="$(cat /sys/class/net/"$I"/statistics/rx_bytes)"; T2="$(cat /sys/class/net/"$I"/statistics/tx_bytes)"; DLS=$((($R2-$R1)/$SLP)); ULS=$((($T2-$T1)/$SLP)); printf "d%s KB/s u%s KB/s" "$(($DLS/1024))" "$(($ULS/1024))" ;} #for total="$((($DLS+$ULS)/1024))" 
#wsig() { sig="$(tail -n 1 /proc/net/wireless | tr -ds '.' '[:space:]' | cut -d' ' -f3)" && perc="$(( sig * 100 / 70 ))" && printf "%d%%\n" "$perc" ;}
#ssid() { printf "%s\n" "$(iw dev | grep ssid | awk '{print $2}')" ;} #iwgetid | cut -d'"' -f2
#cpu() { printf "c%s\n" "$(top -b -n 1 | grep Cpu | tr -s ' ' | cut -d' ' -f3 | sed 's/\[.*$/%/')" ;}
##CPU Load: CPU_LOAD=$(top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1"%"}') # percentage of CPU in use 
#hdd() { printf "h%s" "$(df -h / | awk '/dev/ {print $3}' | grep G)" ;}
#mem() { printf "r%s" "$(free -h | awk '/Mem:/ {print $3}')" ;}
##MEM_LOAD=$(free | grep Mem | awk '{print $3/$2 * 100.0}') # percentage of Memory in use 
##cpu() { val="$( ( cat /proc/stat; sleep 0.3; cat /proc/stat ) | awk '/^cpu / {usr=$2-usr; sys=$4-sys; idle=$5-idle; iow=$6-iow} END {total=usr+sys+idle+iow; printf "%.2f\n", (total-idle)*100/total}')" && printf "c%s" "$val" ;}
#temp() { printf "%s" "$(sensors | grep temp1 | cut -d'+' -f2 )" ;}
#bri() { printf "*%s" "$(brightnessctl | grep % | sed 's/^.*(//;s/)$//')" ;}
#vol() { printf "^%s%s" "$(amixer sget Master | tail -n 1 | sed 's/\] \[.*// ; s/^.*\[//')" "$(amixer sget Master | tail -n 1 | cut -d\[ -f 3 | sed 's/]//')" ;}
#bat() { printf "~%s%%" "$(cat /sys/class/power_supply/BAT0/capacity)" ;}
#tdate() { printf "%s\n" "$(date +"%a%d%b%y %H:%M")" ;}
#
##https://askubuntu.com/questions/211837/how-can-i-get-the-essid-name-only-that-i-connected-with
##essid name:iwconfig wlan0 2> /dev/null | awk -F\" '{print $2}'
##iw dev | grep ssid | awk '{print $2}' #iwgetid | cut -d'"' -f2
##access point mode:iwconfig wlan0 2> /dev/null | awk -F: '/Mode:/ {print $2}' | awk '{print $1}'
##link Quality :iwconfig wlan0 2> /dev/null | awk -F= '/Quality/ {print $2}' | awk '{print $1}'
##channel :sudo iwlist wlan0 scanning essid "ESSID" | grep Channel | head -1 | awk -F: '{print $2}'
##address :ifconfig wlan0 2> /dev/null | awk -F: '/inet\ addr/ {print $2}' | awk '{print $1}'
#
#while sleep 0.8; do echo "$(netsp)  $(ssid)_$(wsig)  $(cpu) $(hdd) $(mem) $(temp)  $(bri) $(vol) $(bat)  $(tdate)" ;done
slstatus -s &
