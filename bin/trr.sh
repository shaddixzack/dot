#!/bin/sh

url="https://thepiratebay.party/search/'${*}'" ; [ -z "${*}" ] && exit 1 
page="$(wget -qO- "$url" | sed 's@+@ @g;s@%@\\x@g' | xargs -0 printf "%b")"
list="$(printf '%s\n' "$page" | grep magnet | grep -v '<meta' | sed 's/href="magnet/\nmagnet/g' | grep magnet | cut -d\" -f1 | awk -F\& '{print $2 "|" $1}' | tr "+" " " | sed 's/dn=//g')"
sel="$(printf '%s\n' "$list" | fzf)" ; [ -z "$sel" ] && exit 1 

#display File Size and Seeds
title="$(printf '%s\n' "$sel" | cut -d\| -f1)"
printf '%s\n' "$title"
printf "Size\t\tSeeds\n"
printf '%s\n' "$page" | grep "$title" -A2 | tail -n2 | sed 's/<td align="right">//g;s/<\/td>//g;s/&nbsp;/ /g' | dos2unix | sed ':a;N;$!ba;s/\n/ \t /g'

magnet="$(printf '%s\n' "$sel"|cut -d\| -f2)" 
#printf "\r\ntransmission-remote -a %s\n" "$magnet" >> "$HOME"/.bash_history
transmission-remote -a "$magnet"
