#!/bin/sh

chosen="$(printf "shutdown\nreboot\nlock\nsuspend\nlogout" | rofi -p "POWER OPTION!!!" -dmenu -i -selected-row 2)"
case "$chosen" in
    "shutdown")	systemctl poweroff ;;
    "reboot") systemctl reboot ;;
    "lock") betterlockscreen -l ;;
    "suspend") amixer set Master mute; systemctl suspend ;;
    "logout") bspc quit ;;
esac
